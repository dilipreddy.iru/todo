import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
 
import Home from './components/login';
import ToDo from './components/toDo';

 
class App extends Component {
  render() {
    return (      
       <BrowserRouter>
        <div>
            <Switch>
             <Route path="/" component={Home} exact/>
             <Route path="/todo" component={ToDo}/>
           </Switch>
        </div> 
      </BrowserRouter>
    );
  }
}
 
export default App;