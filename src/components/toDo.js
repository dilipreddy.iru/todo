import React, { Component } from "react";
import axios from 'axios'
import TodoInput from "../components/TodoInput";
import TodoList from "../components/TodoList";
import "bootstrap/dist/css/bootstrap.min.css";
import uuid from "uuid";
class TODO extends Component {
  state = {
    items: [],
    id: "",
    item: "",
    editItem: false
  };
  
  handleChange = e => {
    console.log(' e.target.value',  e.target.value);
    this.setState({
      item: e.target.value
    });
    
    
  };
  handleSubmit = e => {
    e.preventDefault();
    
    const newItem = {
      // id: this.state.id,
      title: this.state.item
    };
    // console.log(newItem.title);
    if(newItem.title){
      axios.post('/todos', newItem)
    .then(function (response) {
      if(!response.statusText){
        console.log("Something Went Wrong");
      } else{
        window.location.reload(); 
      }
     
    })
    .catch(function (error) {
      console.log(error);
    });
    }else{
      console.log("please enter value");
    }
    
    const updatedItems = [...this.state.items, newItem];

    this.setState({
      items: updatedItems,
      item: "",
      id: uuid(),
      editItem: false
    });
  };

  
  sayEdit = id => {
    // e.preventDefault()
    console.log('iddd', id);
    
    console.log('title', this.state.item);
    let data = [{
      _id:"5eeae2e6dcb5445634579ee6",
      title: this.state.item
    }]
        // const updatedItems = [...this.state.items, id];
    // this.setState({id: updatedItems})
    const filteredItems = this.state.items.filter(item => item._id !== id);

    console.log('filteredItems', this.state.items);
    axios
    .patch(`/todos/${this.state.id}`, {title: this.state.item})
    .then(res => {
      console.log("hey",res)
      //  this.setState({item : res.data.title})
    })
    .catch(err => {
      console.log(err)
    })
  }
  clearList = () => {
    this.setState({
      items: []
    });
  };
  handleDelete = id => {
    let self = this;
    const filteredItems = this.state.items.filter(item => item._id !== id);
    axios.delete(`/todos/${id}`, { params: { filteredItems } }).then(response => {
      if(!response.statusText){
        console.log("Something Went wrong");
      }
      else{
        window.location.reload(); 
      }
      
    });
    this.setState({
      items: filteredItems
    });
  };
  componentDidMount(){
    this.setState({
      editItem: false
    })
  }
  handleEdit = id => {
   
    const updatedItems = [...this.state.items, id];
    console.log('updatedItems', updatedItems);
    this.setState({id: updatedItems})
    axios
    .get(`/todos/${updatedItems}`)
    .then(res => {
      console.log("hey",res)
       this.setState({item : res.data.title})
    })
    .catch(err => {
      console.log(err)
    })

    this.setState({
      // items: filteredItems,
      editItem: true,
      // id: id
    });
  };
  render() {
    
    return (
      <div className="container">
        <div className="row">
          <div className="col-10 mx-auto col-md-8 mt-4">
            <h3 className="text-capitalize text-center">Add Your TODO Here</h3>
            <TodoInput
              item={this.state.item}
              handleChange={this.handleChange}
              handleSubmit={this.handleSubmit}
              sayEdit = {this.sayEdit}
              editItem={this.state.editItem}
            />
            <TodoList
              items={this.state.items}
              handleDelete={this.handleDelete}
              handleEdit={this.handleEdit}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default TODO;
