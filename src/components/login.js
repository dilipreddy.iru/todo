import React, { Component } from "react";
import axios from 'axios'
import { Form, Input, Button } from 'antd';
export default class TodoList extends Component {
    constructor(props){
        super(props)
        this.state = {
            email: '',
            password: '',
          };
    }
 
  onSubmit = () => {
    const newItem = {
        email: this.state.email,
        password: this.state.password
    }

    console.log(newItem);
    let self = this;
    //  this.props.history.push('/todo');
    if(newItem.email === '' || newItem === ''){
        console.log("email or password field empty");
    }else{
        axios.post('/user/login', newItem)
        .then(function (response) {
          console.log(response.data);
          if(!response.data.token){
            console.log("please login");
          } else{
            self.props.history.push('/todo');
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
    // this.props.history.push('/todo')
    
 }
  handleChangeEmail =(e) => {
      this.setState({
          email: e.target.value
      })
    // console.log(e.target.value);
  }

  handleChangePassword =(e) => {
    this.setState({
        password: e.target.value
    })
  // console.log(e.target.value);
}

  render() {
    return (
        <div className="rows" style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}>
             
            
        <Form
          name="basic"
        >
          <Form.Item
            label="Username"
            name="username"
            rules={[
              {
                required: true,
                message: 'Please input your username!',
              },
            ]}
          >
            <Input value={this.state.email} onChange={this.handleChangeEmail}/>
          </Form.Item>
    
          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password value={this.state.password} onChange={this.handleChangePassword}/>
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" onClick={this.onSubmit} style={{margin: 10}}>
              Submit
            </Button>
          </Form.Item>
        </Form>
        </div>
      );
  }
}
