import React, { Component } from "react";
import axios from 'axios'
import TodoItem from "./TodoItem";
export default class TodoList extends Component {
  state = {
    toDolist: []
  };

  componentDidMount(){
    axios
    .get('/todos')
    .then(res => {
      console.log(res)
      this.setState({toDolist : res.data})
    })
    .catch(err => {
      console.log(err)
    })
  }
  render() {
    const { handleDelete, handleEdit } = this.props;
    return (
      <ul className="list-group my-5">
        <h3 className="text-capitalize text-center">Your ToDo list</h3>
        {this.state.toDolist.map(item => {
          return (
            <TodoItem
              key={item._id}
              title={item.title}
              handleDelete={() => handleDelete(item._id)}
              handleEdit={() => handleEdit(item._id)}
            />
          );
        })}
      </ul>
    );
  }
}
